var bloomers = [];

function setup() {
  createCanvas(displayWidth, displayHeight);

  // set up the sound stuff
  setupSound();
}

function draw() {
  background(0);

  // map the "drawBloomer" and "updateBloomer" functions over the bloomer array
  // although these functions don't do anything yet - you need to write them (see below)
  bloomers.map(drawBloomer);
  bloomers.map(updateBloomer);
}

function mouseReleased() {
  // write some code here that pushes a new bloomer to the array
}

function drawBloomer(b) {
  // code which draws the bloomer goes in here
  // the bloomer object will be passed in as the `b` parameter
}

function updateBloomer(b) {
  // all code which updates parameters in the bloom goes in here
  // the bloomer object will be passed in as the `b` parameter

  // write some code that makes the bloomer grow

  // write a condition that makes the bloomer bloom
}
